import React from "react";

import Header from "./Header.js";

import Data from "../testData.json";

import contestpreview from "./contestpreview.js";

class App extends React.Component {
    state = {
        pageheader: "naming test",
        contests: []

    };
    componentDidMount() {
        this.setState({
            contests: Data.contests
        })
    }
    
    render() {
        return (
            console.log(contestpreview),
        <div>    
            <div>
                <Header message={this.state.pageheader}/>
            </div>
            <div>
                {this.state.contests.map(contest =>{
                     <contestpreview key={contest.id} {...contest} />
                })}
               
            </div>
        </div>
        );
    }
}

export default App;