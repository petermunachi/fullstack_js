import express from 'express';
const server = express();

server.set('view engine', 'ejs');

server.use(express.static('public'))

server.get('/', (req, res)=>{
    res.render('index', {
        content: ''
    });
});

server.listen(3000, ()=>{
    console.log('express listening on port', 3000);
    
})